package bo;


public class Account {
    private String CorrectUser;
    private String UncorrectUser;

    public Account(String correctUser,String uncorrectUser){
        CorrectUser = correctUser;
        UncorrectUser = uncorrectUser;
    }

    public String getCorrectUser() {
        return CorrectUser;
    }

    public String getUncorrectUser() {
        return UncorrectUser;
    }
}
