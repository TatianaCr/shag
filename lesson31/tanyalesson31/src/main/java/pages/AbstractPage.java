package pages;

import org.openqa.selenium.WebDriver;
import wd.WebDriverWrapper;

public class AbstractPage {
    static WebDriver driver;

    public AbstractPage() {
        driver = WebDriverWrapper.getInstance();
    }
}
