package builder;

import bo.Letter;
import java.util.Random;

public class LetterBuilder {
    public static Letter createLetter(){
        String recepient = "shagautomation@yandex.ru";
        String subject = "test letter " + new Random().nextInt(99999);
        String body = "You won " + new Random().nextInt(99999) + " dollars!!!";
        return new Letter(recepient, subject, body);
    }
}
