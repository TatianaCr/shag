package testemail;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.EmailInboxPage;
import pages.EmailPage;

public class EmailTest {
    @Test(description = "Getting yandex email test")
    public void gettingYandexEmailTest() throws Exception {
        EmailInboxPage emailPage = new EmailPage().open()
                .inputLogin("shagautomation").inputPassword("shagautomation1")
                .clickLoginButton().getEmail();
        //Assert.assertTrue(emailPage.isEmailPresent(), "Email shagautomation@yandex.ru is present");
        Assert.assertTrue(emailPage.isEmailPresent());
    }
}
