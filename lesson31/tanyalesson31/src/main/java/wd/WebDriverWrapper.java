package wd;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class WebDriverWrapper {
    public WebDriverWrapper() {
    }

    private static WebDriver instance;

    public static WebDriver getInstance(){
        if (instance == null) {
            return init();
        }
        else return instance;
    }

    public static WebDriver init() {
        System.setProperty("webdriver.chrome.driver", new File("src\\main\\resources\\chromedriver.exe").getAbsolutePath());
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }
}
