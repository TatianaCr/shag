package pages;

import org.openqa.selenium.By;
public class EmailInboxPage extends AbstractPage{
    private static final By WRITE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final By SEND_BUTTON_LOCATOR = By.cssSelector(".js-send");
    private static final By FIELD_ADDRESS_LOCATOR = By.xpath("//div[@name='to']");
    private static final By EMAIL_LOCATOR = By.xpath("//div[@class=\"ns-view-container-desc mail-MessagesList js-messages-list\"]//*[@title=\'shagautomation@yandex.ru\']");
    private static final String BASE_URL = "https://mail.yandex.by";

    public boolean isEmailPresent() {
        return driver.findElement(EMAIL_LOCATOR).isDisplayed();
    }

    public EmailInboxPage getEmail(){
        driver.findElement(WRITE_BUTTON_LOCATOR).click();
        driver.findElement(FIELD_ADDRESS_LOCATOR).sendKeys("shagautomation@yandex.ru");
        driver.findElement(SEND_BUTTON_LOCATOR).click();
        //driver.get(BASE_URL + "/?ncrnd=5792&uid=485883837&login=shagautomation#inbox");
        return new EmailInboxPage();
    }
}
