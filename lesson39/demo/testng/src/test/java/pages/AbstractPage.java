package pages;

import browser.Browser;


public class AbstractPage {

    protected static Browser browser;

    public AbstractPage() {
        browser = Browser.get();
    }
}
