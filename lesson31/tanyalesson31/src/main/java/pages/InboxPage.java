package pages;
import org.openqa.selenium.By;
import bo.Letter;

public class InboxPage extends BasePage{
    private static final By LOGIN_NAME_LOCATOR = By.xpath("//div[@data-key=\"view=head-user\"]/div[@class=\"mail-User-Name\"]");
    private static final By NEW_LETTER_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final By NEW_LETTER_LOCATOR = By.xpath("//div[@class=\"ns-view-container-desc mail-MessagesList js-messages-list\"]//*[@title=\'shagautomation@yandex.ru\']");
    public static boolean isLoginNameDisplayed() {
        return driver.findElement(LOGIN_NAME_LOCATOR).isDisplayed();
    }

    public ComposePage clickNewLetter() {
        driver.findElement(NEW_LETTER_BUTTON_LOCATOR).click();
        return new ComposePage();
    }
    public boolean isLetterPresent(Letter letter) {
        return driver.findElement(NEW_LETTER_LOCATOR).isDisplayed();
    }
}
